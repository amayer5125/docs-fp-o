# Fedora Docs Build Repository

This is the build repository for the Fedora Documentation Website.
Fedora Docs are built using [Antora](https://antora.org)

Want to contribute? [Learn about contributing to the Fedora Docs](https://docs.fedoraproject.org/en-US/fedora-docs/contributing/)!

## Reporting issues

Please report general Issues and submit Pull Requests for **Publishing Fixes** here.
Content issues be reported against the source repositories:

* [Documentation Guide](https://pagure.io/fedora-docs/documentation-guide)
* [Installation Guide](https://pagure.io/fedora-docs/install-guide)
* [Release Notes](https://pagure.io/fedora-docs/release-notes)
* [System Administrators Guide](https://pagure.io/fedora-docs/system-administrators-guide)
* [Fedora Council Docs](https://pagure.io/Fedora-Council/council-docs)
* [Quick Docs](https://pagure.io/fedora-docs/quick-docs)
* [Mentored Projects](https://pagure.io/mentored-projects/)
* [Fedora Modularity](https://pagure.io/fedora-docs/modularity)
* [Remix Building](https://pagure.io/fedora-docs/remix-building)
* [CommOps](https://pagure.io/fedora-commops)
* [Packaging Guidelines](https://pagure.io/packaging-committee)
* [Release Specific Information/Navigation Pages](https://pagure.io/fedora-docs/release-docs-home)

## Documentation content

Documentation content is stored in multiple repositories listed in the `site.yml` file in either the `prod` branch for production, or the `stg` branch for staging.

## Testing your changes locally

Both scripts work on Fedora (using Podman) and macOS (using Docker).

**To build the site**, run:

```
$ ./build.sh
```

Please note the build script uses Docker to build the site in an insolated environment.
You might be asked for a root password in order to run it.

The result will be in a `./public/` directory.

**To preview the site**, either open the `./public/en_US/index.html` file in a web browser, or run:

```
$ ./preview.sh
```

### Branching New Releaess

TBD

## Project Plan

See the Project Taiga Board at: https://teams.fedoraproject.org/project/asamalik-antora-for-docs/epics
